module.exports = {
    "setupFiles": [
        "<rootDir>/globalMocks.js"
    ]
}

// Define Test environment variables
process.env = Object.assign(process.env, {
    NODE_ENV: 'test',
    MODE: 'development',
    JWT_PRIVATE_KEY: "TESTING"
});