const request = require('supertest');
const app = require('../../app');
const TestHelper = require('../../util/testHelper');

describe('Logs', function() {
    test('GET /logs', async () => {
      let res = await request(app).get("/logs").set('Accept', 'application/json');
      expect(res.statusCode).toBe(200);
      TestHelper.expectBodyArrayContains(res, 'log');
    });
});

