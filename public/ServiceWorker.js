
// Static Variables
let PRECACHE = `precache-v0.2:4`;
let PRECACHE_URLS = ["/js/main.min.js","/css/main.min.css","/manifest.json","/icon/android-icon-192x192.png","/icon/android-icon-192x192_maskable.png","/icon/apple-icon-114x114.png","/icon/apple-icon-114x114_maskable.png","/icon/apple-icon-120x120.png","/icon/apple-icon-120x120_maskable.png","/icon/apple-icon-144x144.png","/icon/apple-icon-144x144_maskable.png","/icon/apple-icon-152x152.png","/icon/apple-icon-152x152_maskable.png","/icon/apple-icon-180x180.png","/icon/apple-icon-180x180_maskable.png","/icon/apple-icon-57x57.png","/icon/apple-icon-60x60.png","/icon/apple-icon-72x72.png","/icon/apple-icon-72x72_maskable.png","/icon/apple-icon-76x76.png","/icon/apple-icon-76x76_maskable.png","/icon/favicon-16x16.png","/icon/favicon-32x32.png","/icon/favicon-96x96.png","/icon/favicon-96x96_maskable.png","/icon/favicon.png","/offline"];
let OFFLINE_URL = "/offline";

// Install
self.addEventListener('install', event => {
    event.waitUntil(
        caches.open(PRECACHE).then(cache => { 
            cache.addAll(PRECACHE_URLS);
        }).then(
            self.skipWaiting()
        )
    );
});

// Activate
self.addEventListener('activate', event => {
    const currentCaches = [PRECACHE];
    event.waitUntil(
        caches.keys().then(cacheNames => {
            return cacheNames.filter(cacheName => !currentCaches.includes(cacheName));
        }).then(cachesToDelete => {
            return Promise.all(cachesToDelete.map(cacheToDelete => {
                return caches.delete(cacheToDelete);
            }));
        }).then(() => self.clients.claim())
    );
});

// Fetch Handler
let originLength = self.location.origin.length;
self.addEventListener('fetch', event => {
    // Handle Caching Event (only for GET requests)
    if (event.request.method == "GET" && PRECACHE_URLS.includes(event.request.url.substring(originLength))) {
        event.respondWith(
            caches.match(event.request).then(cachedResponse => {
                if (cachedResponse) {
                    return cachedResponse;
                }
                else {
                    // Fallback: fetch manually if cachedResponse is null
                    return fetch(event.request).then(response => {
                        return response;
                    });
                }
            })
        );
    }
    // Handle Offline Page
    else if (OFFLINE_URL.length != null && navigator && !navigator.onLine) {
        event.respondWith(caches.match(OFFLINE_URL));
    }
    // Request not chached and not offline (let the browser handle the request)
    else {
        return;
    }
});
