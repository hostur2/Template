const fs = require('fs');

module.exports.getVersion = () => {
    const version = JSON.parse(fs.readFileSync(`./public/src/version.json`, 'utf8'));
    return version;
}

module.exports.getVersionText = () => {
    const version = JSON.parse(fs.readFileSync(`./public/src/version.json`, 'utf8'));
    return `${version.version}:${version.build}`;
}