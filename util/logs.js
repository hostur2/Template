const version = require("./version");

const LCERROR = '\x1b[31m%s\x1b[0m'; //red
const LCWARN = '\x1b[33m%s\x1b[0m'; //yellow
const LCINFO = '\x1b[36m%s\x1b[0m'; //cyan
const LCDB = '\x1b[34m%s\x1b[0m'; //blue
const LCSUCCESS = '\x1b[32m%s\x1b[0m'; //green

// Write a log entry to the database and display in the console in development mode (warning = yellow, error = red, info = blue, success = green, database = )
const writeLog = async (message, stack, type = "info") => {
    
    // Prevent recursive calls when logging sequelize messages
    if (message.includes("INSERT INTO `log`")) {
        return;
    }

    // Get Type code
    let typeCode = "";
    switch (type) {
        case "error":
            typeCode = `[ CRITICAL ] `;
            break;
        case "warning":
            typeCode = `[   HINT   ] `;
            break;
        case "info":
            typeCode = `[   INFO   ] `;
            break;
        case "server":
            typeCode = `[  SERVER  ] `;
            break;
        case "database":
            typeCode = `[ DATABASE ] `;
            break;
    }

    // Log to console
    let logMessage = `${new Date().toISOString().substring(0, 10)} ${new Date().toLocaleTimeString("de-DE", {
        hour12: false,
        hour: "2-digit",
        minute: "2-digit"
    })} ${typeCode} ${message}`;

    switch (type) {
        case "error":
            console.log(LCERROR, logMessage);
            break;
        case "warning":
            console.log(LCWARN, logMessage);
            break;
        case "server":
            console.log(LCSUCCESS, logMessage);
            break;
        case "database":
            console.log(LCDB, logMessage);
            break;
        default:
            console.log(LCINFO, logMessage);
            break;
    }

    // Log into DB
    if (process.env.DB_NAME) {
        const { Log } = require('../db/models');
        await Log.create({
            version: version.getVersionText(),
            message: message,
            stack: stack == "object" ? JSON.stringify(stack) : stack,
            type: type,
            environment: process.env.ENVIRONMENT
        });
    }
}

// write log entry from error (usign writeLog function)
const writeErrorLog = async (error, message) => {
    if (!message) {
        message = error.message;
    }
    await writeLog(message, error.stack, "error");
};

module.exports = {
    writeLog,
    writeErrorLog
}

if (process.env.DB_NAME) {
    const { Op } = require("sequelize");
    
    // Automatically clear logs after 24 hours (checked every hour)
    let timeoutHours = 2;
    setInterval(async () => {
        await Log.destroy({
            where: {
                createdAt: {
                    [Op.lt]: new Date(Date.now() - (timeoutHours * 60 * 60 * 1000))
                },
                environment: process.env.ENVIRONMENT,
                type: {
                    [Op.in]: ["info", "server", "database"]
                }
            }
        });
        writeLog("Logs cleared", "", "server");
    }, 60 * 60 * 1000);
}