// Mock Logging to prevent useless loges in db and console
const writeLog = async (message, stack, type = "info") => { return; }
const writeErrorLog = async (error, message) => { return; };

module.exports = {
    writeLog,
    writeErrorLog
}