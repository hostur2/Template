const fs = require('fs');

const getArrayResponse = async (name) => {
    return [await getResponse(name)];
}

const getResponse = async (name) => {
    let def = require(`../db/definitions/${name.toLowerCase()}`);
    
    let response = {};
    for (const key in def.definition) {
        const element = def.definition[key];
        if (element.mock) {
            response[key] = element.mock;
        }
    }

    return response;
}

const expectBodyContains = (res, name, additionalKeys = []) => {
    expectBodyContainsKeys(res.body, name, additionalKeys);
}

const expectBodyArrayContains = (res, name, additionalKeys = []) => {
    expectBodyContainsKeys(res.body[0], name, additionalKeys);
}

const expectBodyContainsKeys = (body, name, additionalKeys = []) => {
    let def = require(`../db/definitions/${name.toLowerCase()}`);
    
    for (const key in def.definition) {
        const element = def.definition[key];
        if (element.mock) {
            if (element.mock != "{{NOW}}") {
                expect(body[key]).toBe(element.mock);
            }
        }
    }

    // Check if no other keys are in the response
    for (const key in body) {
        if (!["id", "createdAt", "updatedAt"].includes(key) && !additionalKeys.includes(key)) {
            expect(def.definition[key]).toBeDefined();
        }
    }
}

module.exports = {
    getResponse,
    getArrayResponse,
    expectBodyContains,
    expectBodyArrayContains
}