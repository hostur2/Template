// Wrapper for Logs util wich will not cause a error if used inside www (because Sequelized is not jet loaded)
let Logs = null;

const writeLog = async (message, stack, type = "info") => {
    if (!Logs) {
        Logs = require('../util/logs');
    }
    Logs.writeLog(message, stack, type);
}

const writeErrorLog = async (error, message) => {
    if (!Logs) {
        Logs = require('../util/logs');
    }
    Logs.writeErrorLog(error, message);
}

module.exports = {
    writeLog,
    writeErrorLog
}