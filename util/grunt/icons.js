const fs = require('fs');
const sharp = require('sharp');

/**
 * Generates icons for the app
 * @param {Array} icons - The list of icons to generate
 * @param {string} icons.name - The name of the icon
 * @param {number} icons.size - The size of the icon
 * @param {boolean} icons.maskable - Whether the icon should also be generated with _maskable suffix
 * @param {number} maskable_scale - The scale of the maskable icon
 */

module.exports = function() {
    // variables
    let options = this.options({
        sourceIcon: 'public/icon/favicon.png',
        outPath: 'public/icon/',
        icons: [],
        maskable_scale: 1,
        maskable_suffix: '_maskable'
    });
    
    // check if source icon exists
    if (!fs.existsSync(options.sourceIcon)) {
        throw new Error(`Source icon ${options.sourceIcon} does not exist`);
    }
    
    // Generate icons
    options.icons.forEach(icon => {
       GenerateIcon(options, icon.name, icon.size);
       if (icon.maskable) {
           GenerateIcon(options, icon.name, icon.size, options.maskable_scale);
       }
    });
    
    return;
}

// Generate an icon
// use sharp to resize the icon to size
// if scale is not 1, use sharp to resize the icon to size * scale but keep the original size
// save the icon to outPath + name
function GenerateIcon(options, name, size, scale = 1) {
    let outPath = options.outPath + name;
    let isMaskable = scale != 1;
    if (isMaskable) {
        let ext = outPath.split('.').pop();
        outPath = outPath.replace('.' + ext, options.maskable_suffix + '.' + ext);
    }
    
    // check if file exists and delete it if it does
    if (fs.existsSync(outPath)) {
        fs.rmSync(outPath);
    }
    
    
    // Calculate scale as int
    let pixelScale = Math.round(scale * size);
    let pixelScaleBorderStart = Math.round((size - pixelScale) / 2);
    let pixelScaleBorderEnd = size - pixelScale - pixelScaleBorderStart;
    
    // Get source icon
    let sourceIcon = fs.readFileSync(options.sourceIcon);
    
    // Resize icon
    let icon = sharp(sourceIcon);
    icon = icon.resize(size, size);
    
    // If scale is not 1, resize the icon again
    if (scale != 1) {
        icon.resize(pixelScale, pixelScale);
        icon.extend({
            top: pixelScaleBorderStart,
            bottom: pixelScaleBorderEnd,
            left: pixelScaleBorderStart,
            right: pixelScaleBorderEnd,
            background: { r: 0, g: 0, b: 0, alpha: 0 }
        });
    }
    icon.toFile(outPath);
};