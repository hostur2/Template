const e = require('express');
const fs = require('fs');

// Execute DB mocka and definition generator
module.exports = async function() {
    let done = this.async();

    // variables
    let options = this.options({
        srcPath: 'db/definitions/',
        file: 'erd.md'
    });

    // Get All Files
    let filePaths = getAllFiles(options.srcPath);

    // Get all DB definitions
    let dbDefinitions = [];
    for (let i = 0; i < filePaths.length; i++) {
        dbDefinitions.push(require(`../../${filePaths[i].replace(".js", "")}`));
        dbDefinitions[i].file = fs.readFileSync(filePaths[i], 'utf8');
    }
    
    // Create all ERD md definitions
    let erdDefinitions = [];
    for (const dbDefinition of dbDefinitions) {
        erdDefinitions.push(CreateERDClass(dbDefinition, options));
    }
    for (const dbDefinition of dbDefinitions) {
        erdDefinitions.push(CreateERDProperties(dbDefinition, options));
    }
    for (const dbDefinition of dbDefinitions) {
        erdDefinitions.push(CreateERDForeigenKeys(dbDefinition, options));
    }
    
    // Create ERD md overview
    await CreateERDOverview(erdDefinitions, options);

    done();
}

// Create ERD md definition
function CreateERDClass(dbDefinition, options) {
    let name = dbDefinition.name.name;
    let outFile = `class ${dbDefinition.name.name}\n`;
    
    return outFile;
}

function CreateERDProperties(dbDefinition, options) {
    let name = dbDefinition.name.name;
    let outFile = ``;
    
    // Generate attributes
    for (const def in dbDefinition.definition) {
        let definitionValue = dbDefinition.definition[def];
        
        if (definitionValue.type == 'VIRTUAL') {
            outFile += `${name} : ${def}()\n`;
        }
        else {
            outFile += `${name} : ${def} ${definitionValue.type}\n`;    
        }
    }
    
    return outFile;
}

function CreateERDForeigenKeys(dbDefinition, options) {
    let name = dbDefinition.name.name;
    let outFile = ``;
    
    // Generate relations
    if (dbDefinition.relations) {
        for (const relation of dbDefinition.relations) {
            if (relation.type == 'hasMany' || relation.type == 'hasOne') {
                outFile += `${relation.model} : [FK] ${relation.foreignKey} INTEGER\n`;     
            }
            
            if (relation.through) { 
                outFile += `${relation.through} : [PK] ${relation.foreignKey} INTEGER\n`;
                outFile += `${name} --|> ${relation.through} : ${relation.as}\n`;
            }
            else if (relation.type == 'hasMany') {
                outFile += `${name} --|> ${relation.model} : ${relation.as}\n`;
            }
            else if (relation.type == 'hasOne') {
                outFile += `${name} --|> ${relation.model} : ${relation.as}\n`;
            }
        }
    }
    
    return outFile;
}

// Create ERD md overview
async function CreateERDOverview(erdDefinitions, options) {
    let outFile = "```mermaid\n";
    outFile += "classDiagram\n";
    
    for (const erdDefinition of erdDefinitions) {
        outFile += erdDefinition;
    }
    
    outFile += "```";
    
    fs.writeFileSync(`${options.file}`, outFile);
}

// Read Files recursively
function getAllFiles(dirPath, arrayOfFiles) {
    let files = fs.readdirSync(dirPath);
  
    arrayOfFiles = arrayOfFiles || [];
  
    files.forEach(function(file) {
        if (fs.statSync(dirPath + "/" + file).isDirectory()) {
            if (!file.includes('__mocks__')) {
                arrayOfFiles = getAllFiles(dirPath + "/" + file, arrayOfFiles);
            }
        } else {
            arrayOfFiles.push(`${dirPath}/${file}`);
        }
    });
  
    return arrayOfFiles.filter(path => path.match(/\.js$/));
}