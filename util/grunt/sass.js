const fs = require('fs');
const sass = require("sass");

module.exports = function() {
    // variables
    let options = {
        publicDir: "/public",
        srcPath: "./public/src/sass",
        outPath: "./public/css",
        sourceMapRef: "\n/*# sourceMappingURL={{name}}.min.css.map */"
    }
    options.srcPath = this.data.srcPath || options.srcPath;
    options.outPath = this.data.outPath || options.outPath;
    options.sourceMapRef = this.data.sourceMapRef || options.sourceMapRef;
    options.file = this.data.options.file;
    if (!options.file) {
        console.error("ERROR: No file specified");
        throw new Error("No file specified");
    }  
    
    // compile
    if (Array.isArray(options.file)) {
        options.file.forEach(file => {
            compile(file, options);
        });
    }
    else { 
        compile(options.file, options);
    }
}

function compile(file, options) {
    let fileName = file.replace(".scss", "").replace(".sass", "");
    const result = sass.compile(`${options.srcPath}/${file}`, {
        sourceMap: true,
        style: 'compressed'
    });

    // Get CSS content and append sourceMap reference
    let css = result.css;
    css += options.sourceMapRef.replace("{{name}}", fileName);

    // Fix malformed SourceMap
    let path = __dirname.replaceAll("\\", "/").replaceAll("/util/grunt", "").replaceAll(" ", "%20") + options.publicDir;
    let srcMap = result.sourceMap;
    srcMap.sources = srcMap.sources.map(p => p.replaceAll("file:///", "").replaceAll(path, ""));

    // Write CSS and SourceMap
    fs.writeFileSync(`${options.outPath}/${fileName}.min.css`, css);
    fs.writeFileSync(`${options.outPath}/${fileName}.min.css.map`, JSON.stringify(srcMap));
}