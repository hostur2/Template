const fs = require('fs');
const { minify } = require("terser");

module.exports = async function() {
    let done = this.async();
    // variables
    let options = {
        srcPath: 'public/src/js',
        outPath: 'public/js',
        relativeSrcPath: '../src/js',
    };
    options.srcPath = this.data.srcPath || options.srcPath;
    options.outPath = this.data.outPath || options.outPath;
    options.relativeSrcPath = this.data.relativeSrcPath || options.relativeSrcPath;
    options.file = this.data.options.file;
    if (!options.file) {
        console.error("ERROR: No file specified");
        throw new Error("No file specified");
    }
    let fileName = options.file.replace(".js", "");

    // Get All Filepaths
    let filePaths = getAllFiles(options.srcPath);

    // Get all files
    let files = {};
    filePaths.forEach(p => {
        files[p.replace(options.srcPath, options.relativeSrcPath)] = fs.readFileSync(p, "utf8");
    });

    // minify
    await minifyFiles(files, options.outPath, fileName);
    done();
}

async function minifyFiles(files, outPath, fileName) {
    let result = await minify(files, {
        sourceMap: {
            filename: `${fileName}.min.js`,
            url: `${fileName}.min.js.map`
        }
    });

    // Save minimized file
    fs.writeFileSync(`${outPath}/${fileName}.min.js`, result.code);
    fs.writeFileSync(`${outPath}/${fileName}.min.js.map`, result.map);

    // return true if finished
    return true; 
}

function getAllFiles(dirPath, arrayOfFiles) {
    let files = fs.readdirSync(dirPath);
  
    arrayOfFiles = arrayOfFiles || [];
  
    files.forEach(function(file) {
        if (fs.statSync(dirPath + "/" + file).isDirectory()) {
            arrayOfFiles = getAllFiles(dirPath + "/" + file, arrayOfFiles);
        } else {
            arrayOfFiles.push(`${dirPath}/${file}`);
        }
    });
  
    return arrayOfFiles.filter(path => path.match(/\.js$/));
}