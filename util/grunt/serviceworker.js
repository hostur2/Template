const fs = require('fs');

/**
 * Generates a ServiceWorker file
 * Use following placeholders:
 * {{VERSION}} - The current version number
 * {{BUILD}} - The current build number
 * {{PRECACHE_URLS}} - The list of URLs to precache (Array)
 * {{OFFLINE_URL}} - The URL to redirect to when offline
 * @param {string} serviceWorkerFile Path to ServiceWorker file to generate
 * @param {string} versionFile Path to the version file to use
 * @param {string} precacheList The list of URLs to precache
 * @param {string} autoCache The list of directories to auto cache
 * @param {string} [serviceWorkerTemplateFile] Path to the ServiceWorker file to use as Template
 * @param {string} [offlineUrl] The URL to redirect to when offline
 */

module.exports = function() {
    // variables
    let options = this.options({
        offlineUrl: "/offline",
        versionFile: 'public/src/version.json',
        serviceWorkerFile: `public/ServiceWorker.js`,
        serviceWorkerTemplateFile: null,
        precacheList: ["/"],
        autoCache: []
    });

    // Get version from file
    let version = { version: "0.1", build: 0 };
    try { version = JSON.parse(fs.readFileSync(options.versionFile, 'utf8')); } catch { }
    let versionNumber = version.version;

    // Get ServiceWorker from file
    let serviceWorkerFile = serviceWorkerTemplate;
    if (options.serviceWorkerTemplateFile) {
        serviceWorkerFile = fs.readFileSync(options.serviceWorkerTemplateFile, 'utf8');
    }

    // Get Build Number
    let buildNumber = version.build;
    buildNumber++;

    // Generate Precache List
    let precacheList = options.precacheList;

    // Add autoCache to precacheList
    options.autoCache.forEach(cachePath => {
        precacheList = precacheList.concat(GetAllFiles(cachePath));
    });
    
    // Add offline URL to precacheList
    if (options.offlineUrl) {
        precacheList.push(options.offlineUrl);   
    }

    // Generate ServiceWorker
    serviceWorkerFile = serviceWorkerFile.replaceAll("{{OFFLINE_URL}}", options.offlineUrl ? options.offlineUrl : "");
    serviceWorkerFile = serviceWorkerFile.replaceAll("{{VERSION}}", versionNumber);
    serviceWorkerFile = serviceWorkerFile.replaceAll("{{BUILD_NUMBER}}", buildNumber);
    serviceWorkerFile = serviceWorkerFile.replaceAll("{{PRECACHE_URLS}}", JSON.stringify(precacheList));

    // Write ServiceWorker to file
    fs.writeFileSync(options.serviceWorkerFile, serviceWorkerFile);

    // Write version to file
    fs.writeFileSync(options.versionFile, JSON.stringify({
        build: buildNumber,
        version: versionNumber
    }));
    return;
}

// Get a list of all full filenames of the files in the given directory (recursive)
const GetAllFiles = dir =>  {
    // Check if directory exists and create if not
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    
    // Get all files in directory
    let files = fs.readdirSync(dir);
    let result = [];
    for (let file of files) {
        let filePath = dir + file;
        filePath = filePath.replaceAll("//", "/");
        if (fs.lstatSync(filePath).isDirectory()) {
            result = result.concat(GetAllFiles(filePath));
        } else {
            result.push(filePath.replaceAll("public/", "/"));
        }
    }
    return result;
}

// Serviceworker Template
const serviceWorkerTemplate = `
// Static Variables
let PRECACHE = \`precache-v{{VERSION}}:{{BUILD_NUMBER}}\`;
let PRECACHE_URLS = {{PRECACHE_URLS}};
let OFFLINE_URL = "{{OFFLINE_URL}}";

// Install
self.addEventListener('install', event => {
    event.waitUntil(
        caches.open(PRECACHE).then(cache => { 
            cache.addAll(PRECACHE_URLS);
        }).then(
            self.skipWaiting()
        )
    );
});

// Activate
self.addEventListener('activate', event => {
    const currentCaches = [PRECACHE];
    event.waitUntil(
        caches.keys().then(cacheNames => {
            return cacheNames.filter(cacheName => !currentCaches.includes(cacheName));
        }).then(cachesToDelete => {
            return Promise.all(cachesToDelete.map(cacheToDelete => {
                return caches.delete(cacheToDelete);
            }));
        }).then(() => self.clients.claim())
    );
});

// Fetch Handler
let originLength = self.location.origin.length;
self.addEventListener('fetch', event => {
    // Handle Caching Event (only for GET requests)
    if (event.request.method == "GET" && PRECACHE_URLS.includes(event.request.url.substring(originLength))) {
        event.respondWith(
            caches.match(event.request).then(cachedResponse => {
                if (cachedResponse) {
                    return cachedResponse;
                }
                else {
                    // Fallback: fetch manually if cachedResponse is null
                    return fetch(event.request).then(response => {
                        return response;
                    });
                }
            })
        );
    }
    // Handle Offline Page
    else if (OFFLINE_URL.length != null && navigator && !navigator.onLine) {
        event.respondWith(caches.match(OFFLINE_URL));
    }
    // Request not chached and not offline (let the browser handle the request)
    else {
        return;
    }
});
`;