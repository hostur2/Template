const fs = require('fs');

const DATE_NOW = "2000-01-01T16:20:00.069Z"

// Execute DB mocka and definition generator
module.exports = async function() {
    let done = this.async();

    // variables
    let options = this.options({
        srcPath: 'db/definitions/',
        outPathModels: 'db/models/',
        outPathMocks: 'db/models/__mocks__',
        outOverviewFile: 'db/models.js',
        GlobalMockFile: 'test/globalMocks.js'
    });

    // Get All Files
    let filePaths = getAllFiles(options.srcPath);

    // Get all DB definitions
    let dbDefinitions = [];
    for (let i = 0; i < filePaths.length; i++) {
        dbDefinitions.push(require(`../../${filePaths[i].replace(".js", "")}`));
        dbDefinitions[i].file = fs.readFileSync(filePaths[i], 'utf8');
    }

    // Create DB models
    for (const dbDefinition of dbDefinitions) {
        await CreateModel(dbDefinition, options);
    }

    // Create DB models overview
    await CreateModelOverview(dbDefinitions, options);

    // Create DB mocks
    for (const dbDefinition of dbDefinitions) {
        await CreateMock(dbDefinition, options);
    }

    // Create globalMocks
    await CreateGlobalMockFile(dbDefinitions, options);

    done();
}

// Create Sequelize Model
async function CreateModel(dbDefinition, options) {
    let outFile = `const Sequelize = require('sequelize');\nconst sequelize = require('../database');`;

    // Generate Model
    outFile += `\n\nconst ${dbDefinition.name.name} = sequelize.define('${dbDefinition.name.dbName}', `;
    outFile += /let definition = ([\s\S]*?\});/gm.exec(dbDefinition.file)[1].replace(/,[\n\t\s]*mock: ([\s\S]*?)\n/gm, "\n");

    // Generate attributes
    outFile += `, `;
    outFile += /let options = ([\s\S]*?\});/gm.exec(dbDefinition.file)[1];
    outFile += `);\n\n`;

    // Generate associations
    outFile += `${dbDefinition.name.name}.associateTo = (models) => {`;
    for (const key in dbDefinition.relations) {
        let rel = dbDefinition.relations[key];
        outFile += `\n\tmodels.${dbDefinition.name.name}.${rel.type}(models.${rel.model}, { `;
        if (rel.through) {
            outFile += `through: models.${rel.through}, `;
        }
        if (rel.foreignKey) {
            outFile += `foreignKey: '${rel.foreignKey}', `;
        }
        if (rel.as) {
            outFile += `as: '${rel.as}', `;
        }
        outFile += `});`;
    }
    outFile += `\n}`;

    // Final line
    outFile += `;\n\nmodule.exports = ${dbDefinition.name.name};`;

    // Write to file
    fs.writeFileSync(`${options.outPathModels}/${dbDefinition.name.name.toLowerCase()}.js`, outFile);
};

// Create Model overview
async function CreateModelOverview(dbDefinitions, options) {
    let outFile = "";
    let names = dbDefinitions.map(definition => definition.name);

    // Generate requires
    outFile += names.map(name => `const ${name.name} = require('./models/${name.name.toLowerCase()}');`).join("\n");

    // Fill Model array
    outFile += `\n\nconst models = {\n\t${names.map(name => `${name.name}`).join(",\n")}\n}`;

    // call Associate To
    outFile += "\n\n// Model relations\n";
    outFile += names.map(name => `${name.name}.associateTo(models);`).join("\n");

    // add final line
    outFile += "\n\nmodule.exports = models;";

    // Write to file
    fs.writeFileSync(options.outOverviewFile, outFile);
}

// Create Mock
async function CreateMock(dbDefinition, options) {
    let outFile = `const SequelizeMock = require("sequelize-mock");\nconst sequelize = new SequelizeMock();`;

    // Generate Model
    outFile += `\n\nconst ${dbDefinition.name.name} = sequelize.define('${dbDefinition.name.dbName}', {`;
    for (const key in dbDefinition.definition) {
        outFile += `\n\t${key}: "${typeof dbDefinition.definition[key].mock === 'string' ? dbDefinition.definition[key].mock.replace("{{NOW}}", DATE_NOW) : dbDefinition.definition[key].mock}",`;
    }
    outFile += `\n});\n\n`;

    // Generate Associations
    outFile += `${dbDefinition.name.name}.associateTo = (models) => {`;
    for (const key in dbDefinition.relations) {
        let rel = dbDefinition.relations[key];
        outFile += `\n\tmodels.${dbDefinition.name.name}.${rel.type}(models.${rel.model}, { `;
        if (rel.through) {
            outFile += `through: models.${rel.through}, `;
        }
        if (rel.foreignKey) {
            outFile += `foreignKey: '${rel.foreignKey}', `;
        }
        if (rel.as) {
            outFile += `as: '${rel.as}', `;
        }
        outFile += `});`;
    }
    outFile += `\n}`;

    // Last Line
    outFile += `;\n\nmodule.exports = ${dbDefinition.name.name};`;
    
    // Write to file
    fs.writeFileSync(`${options.outPathMocks}/${dbDefinition.name.name.toLowerCase()}.js`, outFile);
}

// Create Global Mockfile
/*
// Sequelize Data Models
jest.mock('../models/log');
*/
async function CreateGlobalMockFile(dbDefinitions, options) {
    let outFile = "// Sequelize Data Models\n";

    // Generate mocks
    outFile += dbDefinitions.map(definition => `jest.mock('../${options.outPathModels}${definition.name.name.toLowerCase()}');`).join("\n");

    // Sepcific mocks
    outFile += "\n\n// Specific mocks\n";
    outFile += `jest.mock('../util/logs');`;

    // Write to file
    fs.writeFileSync(`${options.GlobalMockFile}`, outFile);
}

// Read Files recursively
function getAllFiles(dirPath, arrayOfFiles) {
    let files = fs.readdirSync(dirPath);
  
    arrayOfFiles = arrayOfFiles || [];
  
    files.forEach(function(file) {
        if (fs.statSync(dirPath + "/" + file).isDirectory()) {
            if (!file.includes('__mocks__')) {
                arrayOfFiles = getAllFiles(dirPath + "/" + file, arrayOfFiles);
            }
        } else {
            arrayOfFiles.push(`${dirPath}/${file}`);
        }
    });
  
    return arrayOfFiles.filter(path => path.match(/\.js$/));
}