class DBMock {
    constructor() {}

    /**
     * Has Many Relations
     * @param {Sequelize} models DB Model to associate to 
     * @param {Object} options 
     * @param {String} options.foreignKey Foreign key to use
     * @param {String} options.through Table to use for n:n relations
     * @param {String} options.as Alias for the relation
     * @returns 
     */
    hasMany(models, options) {
        return {};
    }

    /**
     * Has One Relations
     * @param {Sequelize} models DB Model to associate to 
     * @param {Object} options 
     * @param {String} options.foreignKey Foreign key to use
     * @param {String} options.through Table to use for n:n relations
     * @param {String} options.as Alias for the relation
     * @returns 
     */
    hasOne(models, options) {
        return {};
    }

    /**
     * Belongs To Many Relations
     * @param {Sequelize} models DB Model to associate to
     * @param {Object} options
     * @param {String} options.foreignKey Foreign key to use
     * @param {String} options.through Table to use for n:n relations
     * @param {String} options.as Alias for the relation
     * @returns
     */
    belongsToMany(models, options) {
        return {};
    }

    /**
     * Belongs To Relations
     * @param {Sequelize} models DB Model to associate to
     * @param {Object} options
     * @param {String} options.foreignKey Foreign key to use
     * @param {String} options.through Table to use for n:n relations
     * @param {String} options.as Alias for the relation
     * @returns
     */
    belongsTo(models, options) {
        return {};
    }
}

new DBMock();
module.exports = DBMock;