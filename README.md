# Project Template
[![Generic badge](https://img.shields.io/badge/16.6.1-Node.js-689f66.svg)](https://shields.io/)
[![Generic badge](https://img.shields.io/badge/4.16.0-express.js-lightgray.svg)](https://shields.io/)
[![Generic badge](https://img.shields.io/badge/6.19.0-Sequelize-blue.svg)](https://shields.io/)
[![Generic badge](https://img.shields.io/badge/27.0.6-Jest-red.svg)](https://shields.io/)
[![Generic badge](https://img.shields.io/badge/3.0.0-Supertest-darkgreen.svg)](https://shields.io/)
[![Generic badge](https://img.shields.io/badge/1.4.1-Grunt-f1a42f.svg)](https://shields.io/)
[![Generic badge](https://img.shields.io/badge/1.51.0-Dart%20Sass-c56293.svg)](https://shields.io/)
[![Generic badge](https://img.shields.io/badge/0.31.3-Sharp-99cc00.svg)](https://shields.io/)


Project Template for a default Webproject using Express and pug to server the website. JS and Sass are automatically compressed and compiled by Grunt taskrunner. Easy to use Sequelize setup wich automatically generates relations, mocks and ERD. Test utilities for Testing mocked DB tables with jest and supertest.

## Features
- Express Server with default Setup
- Sequelize Setup (Automatically generates relations, mocks and ERD)
- Jest and Supertest for Testing (with mocked DB tables)
- Sass Compiler and minfication
- JS bundling and minification
- ServiceWorker generation with built in versioning
- Ready to use logging
- Favicon generation in all sizes (maskable icon  support)
- Docker setup for deployment

## Gitlab Auto DevOps compatible
[![Generic badge](https://img.shields.io/badge/Auto-DevOps-orange.svg)](https://shields.io/)

Environament Variables
```json
{
    "MODE": "development", // "production"
    "PORT": 5000,
    "ENVIRONMENT": "development", // whatever you want
    "ROOT_URL": "http://localhost:5000",
    // DB Configuration (optional)
    "DB_HOST": "127.0.0.1",
    "DB_PORT": 3306,
    "DB_NAME": "template",
    "DB_ADMIN": "root",
    "DB_PASWD": "PASSWORD",
    "DB_DIALECT": "mysql"
}
```

```
K8S_SECRET_MODE: "production" // "development"
K8S_SECRET_ENVIRONMENT: "development" // whatever you want
K8S_SECRET_ROOT_URL: "http://localhost:5000"

K8S_SECRET_DB_HOST: "127.0.0.1"
K8S_SECRET_DB_PORT: 3306
K8S_SECRET_DB_NAME: "template"
K8S_SECRET_DB_ADMIN: "root"
K8S_SECRET_DB_PASWD: "PASSWORD"
K8S_SECRET_DB_DIALECT: "mysql"
```

## Setup
Install Frameworks
```
> npm install
```
Run Server
```npm
> nodemon
```
Run Grunt Watch
```npm
> grunt default
or
> grunt watch
```
Run Testing
```npm
> npm test
```

## Sequelize Definitions
All Datebase tables are to be defined unter db/definitions/ with following structure. All other files will automatically be generated.

```javascript
const Sequelize = require('sequelize');
const DBMock = require('../../util/db/mock');

let name = {
    name: 'Name',
    dbName: 'name',
}

let definition = {
    // Normal Sequelize definition 
    // Additional REQUIRED attribute "mock" defines testvalue for each database field
}

let relations = {
    {
        type: "hasMany", // "hasOne" "belongsTo" "belongsToMany"
        model: "TargetModelName",
        foreignKey: "fkid",
        as: "alias"
    }
}

module.exports = { name, definition, relations, options }
```