const Sequelize = require('sequelize');
const DBMock = require('../../util/db/mock');

let name = {
    name: 'Log',
    dbName: 'log'
}

let definition = {
    version: {
        type: Sequelize.STRING,
        allowNull: false,
        mock: '1.0.0'
    },
    environment: {
        type: Sequelize.STRING,
        allowNull: false,
        mock: 'development'
    },
    type: {
        type: Sequelize.STRING,
        allowNull: false,
        default: 'error',
        validate: {
            isIn: [['error', 'warning', 'info', 'server', 'database']]
        },
        mock: 'error'
    },
    message: {
        type: Sequelize.STRING,
        allowNull: false,
        mock: 'This is a test message'
    },
    stack: {
        type: Sequelize.STRING,
        allowNull: true,
        mock: 'This is a test stack'
    },
    createdAt: { 
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW,
        mock: '{{NOW}}'
    }
};

let options = {
    timestamps: false,
    freezeTableName: true
};

let relations = [];

module.exports = { name, definition, relations, options }