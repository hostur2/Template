const Log = require('./models/log');

const models = {
	Log
}

// Model relations
Log.associateTo(models);

module.exports = models;