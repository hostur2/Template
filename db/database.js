const Sequelize = require('sequelize');
const Logs = require('../util/sequelizeLogs');

var connection;

if (process.env.DB_NAME) {
    if(process.env.DB_SSL_KEY && process.env.DB_SSL_KEY != "") {
        connection = new Sequelize(process.env.DB_NAME, process.env.DB_ADMIN, process.env.DB_PASWD, { 
            host: process.env.DB_HOST, 
            dialect: process.env.DB_DIALECT,
            logging: msg => {
                Logs.writeLog(msg, null, "database");
            },
            dialectOptions: {
                ssl: {
                    key: process.env.DB_SSL_KEY.replace(/\\n/g, '\n'),
                    cert: process.env.DB_SSL_CERT.replace(/\\n/g, '\n'),
                    ca: process.env.DB_SSL_CA.replace(/\\n/g, '\n')
                }
            }
        }, (err) => {
            Logs.writeErrorLog(err, null, "database");
        });
    }
    else {
        connection = new Sequelize(process.env.DB_NAME, process.env.DB_ADMIN, process.env.DB_PASWD, { 
            host: process.env.DB_HOST, 
            dialect: process.env.DB_DIALECT,
            port: process.env.DB_PORT || 3306,
            logging: msg => {
                Logs.writeLog(msg, null, "database");
            }
        }, (err) => {
            Logs.writeErrorLog(err, null, "database");
        });
    }
}

module.exports = connection;