const SequelizeMock = require("sequelize-mock");
const sequelize = new SequelizeMock();

const Log = sequelize.define('log', {
	version: "1.0.0",
	environment: "development",
	type: "error",
	message: "This is a test message",
	stack: "This is a test stack",
	createdAt: "2000-01-01T16:20:00.069Z",
});

Log.associateTo = (models) => {
};

module.exports = Log;