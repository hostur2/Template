const Sequelize = require('sequelize');
const sequelize = require('../database');

const Log = sequelize.define('log', {
    version: {
        type: Sequelize.STRING,
        allowNull: false
    },
    environment: {
        type: Sequelize.STRING,
        allowNull: false
    },
    type: {
        type: Sequelize.STRING,
        allowNull: false,
        default: 'error',
        validate: {
            isIn: [['error', 'warning', 'info', 'server', 'database']]
        }
    },
    message: {
        type: Sequelize.STRING,
        allowNull: false
    },
    stack: {
        type: Sequelize.STRING,
        allowNull: true
    },
    createdAt: { 
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
    }
}, {
    timestamps: false,
    freezeTableName: true
});

Log.associateTo = (models) => {
};

module.exports = Log;