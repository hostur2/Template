const express = require('express');
const router = express.Router();
const version = require('../util/version');

// Metadata
const pageOptions = {
  title: 'Template',
  author: 'Lords of Mahlstrom Gaming',
  description: "Template",
  rootUrl: `${process.env.ROOT_URL}/`,
  bannerUrl: `${process.env.ROOT_URL}/icon/banner.png`,
  version: version.getVersion().version,
  build: version.getVersion().build
};

// GET offline page
router.get('/offline', (req, res) => {
  let opt = structuredClone(pageOptions);
  res.render('offline', opt);
});

// GET home page
router.get('/', function(req, res, next) {
  let opt = structuredClone(pageOptions);
  res.render('index', opt);
});

module.exports = router;

// Polyfill
if (!structuredClone) {
  function structuredClone(obj) {
    return JSON.parse(JSON.stringify(obj));
  }
}