const express = require('express');
const router = express.Router();
const Logs = require('../util/logs');

if (process.env.DB_NAME) {
  const { Op } = require('sequelize');
  const { Log } = require('../db/models');

  // Get logs of the last 24 hours
  router.get('/', function(req, res, next) {
    if (process.env.MODE == "development") {
      Log.findAll({
        where: {
          createdAt: {
            [Op.gte]: new Date(Date.now() - 24 * 60 * 60 * 1000)
          }
        },
        order: [
          ['createdAt', 'DESC']
        ]
      }).then(logs => {
        res.json(logs);
      });
    }
    else {
      res.json([]);
    }
  });
}

module.exports = router;
