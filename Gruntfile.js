module.exports = function(grunt) {
    require('load-grunt-tasks')(grunt);
    
    // Register Custom Tasks
    grunt.registerTask('serviceworker', require('./util/grunt/serviceworker.js'));
    grunt.registerMultiTask('sass', require('./util/grunt/sass.js'));
    grunt.registerMultiTask('jsMin', require('./util/grunt/jsMin.js'));
    grunt.registerTask('mocks', require('./util/grunt/mocks.js'));
    grunt.registerTask('erd', require('./util/grunt/erd.js'));
    grunt.registerTask('icons', require('./util/grunt/icons.js'));
 
    // Config
    grunt.initConfig({
        watch: {
            options: {
                livereload: true
            },
            serviceworker: {
                files: ['public/src/**/*.js', 'public/src/**/*.scss', 'public/src/**/*.sass', 'views/**/*.pug'],
                tasks: ['serviceworker']
            },
            sass: {
                files: ['public/src/sass/**/*.scss', 'public/src/sass/**/*.sass'],
                tasks: ['sass'],
            },
            jsMin: {
                files: ['public/src/js/**/*.js'],
                tasks: ['jsMin'],
            },
            mocks: {
                files: ['db/definitions/**/*.js'],
                tasks: ['mocks'],
            },
            erd: {
                files: ['db/definitions/**/*.js'],
                tasks: ['erd'],
            },
            icons: {
                files: ['public/icon/favicon.png', 'util/grunt/icons.js'],
                tasks: ['icons'],
            }
        },
        serviceworker: {
            options: {
                precacheList: ["/js/main.min.js", "/css/main.min.css", "/manifest.json"],
                autoCache: ["public/img/", "public/icon/"],
            }
        },
        sass: {
            main: {
                options: {
                    file: "main.scss"
                }
            }
        },
        jsMin: {
            main: {
                options: {
                    file: "main.js"
                }
            }
        },
        mocks: {
            options: {
                
            }
        },
        erd: {
            options: {
                file: "erd.md"
            }
        },
        icons: {
            options: {
                icons: [
                    { name: "android-icon-192x192.png", size: 192, maskable: true },
                    { name: "apple-icon-57x57.png", size: 57 },
                    { name: "apple-icon-60x60.png", size: 60 },
                    { name: "apple-icon-72x72.png", size: 72, maskable: true },
                    { name: "apple-icon-76x76.png", size: 76, maskable: true },
                    { name: "apple-icon-114x114.png", size: 114, maskable: true },
                    { name: "apple-icon-120x120.png", size: 120, maskable: true },
                    { name: "apple-icon-144x144.png", size: 144, maskable: true },
                    { name: "apple-icon-152x152.png", size: 152, maskable: true },
                    { name: "apple-icon-180x180.png", size: 180, maskable: true },
                    { name: "favicon-16x16.png", size: 16 },
                    { name: "favicon-32x32.png", size: 32 },
                    { name: "favicon-96x96.png", size: 96, maskable: true }
                ],
                maskable_scale: 0.9, // 90% of the original size
            }
        }
    });
 
    // Init Grunt
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.registerTask('default', ['serviceworker', 'sass', 'jsMin', 'mocks', 'erd', 'watch']);
};